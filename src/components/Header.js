import React from 'react';
import { Navbar, Container } from 'react-bootstrap';

const Header = () => {
    return (
        <Navbar bg="dark" sticky="top" className="shadow">
            <Container className="justify-content-center">
                <h2 className="text-light">
                    SQL Editor
                </h2>
            </Container>
        </Navbar>
    );
};

export default Header;