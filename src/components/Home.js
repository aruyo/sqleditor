import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
import Header from './Header';
import Editor from './Editor';
import Result from './Result';
import executeQuery from '../services/database-service';

const Home = () => {
    const [result, setResult] = useState([]);

    const submitQuery = (query) => {
        try {
            executeQuery(query, (results) => {
                console.log(results);
                if (results.errors.length > 0) {
                    // TODO: show error in alert
                    for (const error of results.errors) {
                        console.error(error.message);
                    }
                    setResult([]);
                } else {
                    setResult(results.data);
                }
            });
        } catch (err) {
            // TODO: show error in alert
            console.error(err.message);
            setResult([]);
        }
    };

    return (
        <div>
            <Header />
            <Container className="mb-5">
                <Editor submitQuery={(query) => submitQuery(query)} />
                <Result result = {result}/>
            </Container>
        </div>
    );
};

export default Home;