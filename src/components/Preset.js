import React from 'react';
import { Badge } from 'react-bootstrap';

const Preset = ({ title, onClick }) => {
    return (
        <div 
            className="me-2" 
            onClick={() => onClick()}
            style={{ cursor: 'pointer' }}>
                <Badge bg="secondary">{title}</Badge>
        </div>
    );
};

export default Preset;