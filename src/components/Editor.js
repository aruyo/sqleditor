import React, { useState } from 'react';
import { 
    ButtonGroup, 
    Button,
    Form,
    FormGroup,
    FormControl
} from 'react-bootstrap';
import Preset from './Preset';
import data from '../data/preset.json';

const Editor = ({ submitQuery }) => {
    const [queries, setQueries] = useState(['']);
    const [active, setActive] = useState(0);
    const [presets, setPresets] = useState(data);

    const setActiveEditor = (idx) => {
        setActive(idx);
    };

    const addEditor = () => {
        const newQueries = queries.concat('');
        setQueries(newQueries);
        setActive(newQueries.length - 1);
    };

    const handleTextarea = (value) => {
        const tempQueries = [...queries];
        tempQueries[active] = value;
        setQueries(tempQueries);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        submitQuery(queries[active]);
    };

    return (
        <div className="rounded shadow my-3 p-3">
            <ButtonGroup aria-label="Editor Tabs">
                {queries.map((query, idx) => (
                    <Button 
                        key={idx}
                        onClick={() => setActiveEditor(idx)}
                        className={`tab-btn rounded-0 border-0 rounded-top px-5 ${idx === active ? "active" : ""}`}>
                            Editor {idx + 1}
                    </Button>
                ))}
                <Button 
                    onClick={() => addEditor()}
                    variant="secondary" 
                    className="add-btn rounded-0 border-0 rounded-top text-center">
                        +
                </Button>
            </ButtonGroup>
            <Form onSubmit={(e) => handleSubmit(e)}>
                <FormGroup>
                    <FormControl 
                        as="textarea" 
                        className="query-textarea rounded-0 rounded-bottom border border-secondary"
                        placeholder="Write your query"
                        value={queries[active]}
                        required={true}
                        onChange={(e) => {handleTextarea(e.target.value)}} />
                </FormGroup>
                <FormGroup className="py-2 text-end">
                    <Button variant="success" type="submit">Execute</Button>
                </FormGroup>
            </Form>
            <h3>Presets</h3>
            <div className="d-flex flex-wrap">
                {presets.map((preset) => {
                    return (
                        <Preset 
                            key={preset.name}
                            title={preset.name} 
                            onClick={() => submitQuery(preset.query)} />
                    );
                })}
            </div>
        </div>
    );
};

export default Editor;