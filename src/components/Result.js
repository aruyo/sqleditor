import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

const Result = (props) => {
    const renderResult = () => {
        if (!props.result) {
            return(
                <div className="spinner-border" role="status">
                </div>
            );
        }

        if (props.result.length === 0){
            return(
                <div className="mt-5">
                    No data found.
                </div>
            );
        }
        return(
            <div className="table-responsive">
                <table className="table table-bordered table-hover mt-4 ">
                    <thead className="thead-dark">
                        <tr>
                            {props.result[0].map((value, index) => {
                                return (
                                    <th scope="col" key={index} className="px-3">{value}</th>
                                )
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {props.result.map((value, index) => {
                            if (index !== 0){
                                return (
                                    <tr key={index} className="px-3">
                                        {props.result[index].map((val, idx) => {
                                            return(
                                                <td className="px-3" key={idx}> {value[idx]} </td>
                                            )
                                        })}
                                    </tr>
                                )
                            }
                        })}
                    </tbody>
                </table>
            </div>      
        );
    };

    return (
        <div className="rounded shadow my-1 p-3">
            <h3 className="mt-5 pl-4 pt-4 font-weight-bold">Query result</h3>
                {renderResult(props.result)}
            </div>
    );
}

export default Result;