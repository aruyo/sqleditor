import Papa from 'papaparse';
import presets from '../data/preset.json';

const executeQuery = (query, callback) => {
    try {
        let filename;
        for (const preset of presets) {
            if (query === preset.query) {
                filename = preset.name;
                break;
            }
        }
        if (filename) {
            loadCsv(`data/${filename}.csv`, callback);
        } else {
            throw Error('Invalid query!');
        }
    } catch (err) {
        throw(err);
    }
};

const loadCsv = (path, callback) => {
    Papa.parse(path, {
        download: true,
        delimiter: ',',
        complete: callback
    })
}

export default executeQuery;