# SQL EDITOR

[![Netlify Status](https://api.netlify.com/api/v1/badges/4f750119-f255-4416-b8ce-699b6f924c07/deploy-status)](https://app.netlify.com/sites/sqleditoraryodev/deploys)

## How to run the project:

In the project directory, you can run:

```npm install```

Install the project with this command.

```npm start```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

How to query for instance:

`SELECT * FROM customers` 

### Calculate App Load Time

The app load time is calculated with React Development Tools provided by browsers that used to open the app. The DevTools will show a “Profiler” tab and could used to record Application load time based on when to start and end the recording time. To test this app load time, we start the recording time before we select one of the SQL predefined queries and end it when the query finished appear on the app. The result of the app time really depends on how much data from SQL queries shown on the app.
It ranges from 3ms to around 300ms when using data from https://github.com/graphql-compose/graphql-compose-examples/tree/master/examples/northwind/data/csv.


App load time when using SQL queries to fetch **regions data**: around 2.8ms

![regions](/public/screenshot/ss-1.PNG)

App load time when using SQL queries to fetch **order details data**: around 248.1ms

![order_details](/public/screenshot/ss-2.PNG)
